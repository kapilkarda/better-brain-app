angular.module('BetterBrain')
.service('SignupService', function ($q,$http,API) {

    function Signup(Email,Password,Gender,FullName,FB_Id){
		  console.log(API.getSignUp(Email,Password,Gender,FullName,FB_Id));
      return $http.get(API.getSignUp(Email,Password,Gender,FullName,FB_Id)).then(function(response){
        return response.data
      });
    }

    return {
      Signup:  Signup
    }

  });