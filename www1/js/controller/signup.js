angular.module('signup.controller', ['ionic'])
.controller('SignupPageController', function($scope, $state, $ionicPlatform, SignupService, $localstorage, $ionicLoading, $ionicHistory, $cordovaFacebook, $http) {
	$ionicPlatform.ready(function(){
		try{
			$scope.Signform={
				"username":"",
				"email":"",
				"password":"",
				"Cpassword":"",
				"gender":""
			};
			$scope.Signform.gender='male';
			$scope.changeGender=function(gender){
				if(gender=='male'){
					$scope.Signform.gender='female';
				}else{
					$scope.Signform.gender='male';
				}
			};	
			
			//Get SignUp
			$scope.DoSignUp = function(){
				if($scope.Signform.username==""){
					$ionicLoading.hide();
					alert("FullName is required.");
					return false;
				}
				if($scope.Signform.email==""){
					$ionicLoading.hide();
					alert("Email is required.");
					return false;
				}else{
					var email=$scope.Signform.email;
					var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if (!filter.test(email))
					{	
						$ionicLoading.hide();
						alert("Please enter Valid email");
						return false;
					}
				}
				if($scope.Signform.password==""){
					$ionicLoading.hide();
					alert("password is required.");
					return false;
				}
				if($scope.Signform.Cpassword==""){
					$ionicLoading.hide();
					alert("password is required.");
					return false;
				}
				if($scope.Signform.gender==""){
					$ionicLoading.hide();
					alert("password is required.");
					return false;
				}
				if($scope.Signform.password==$scope.Signform.Cpassword){
					var FB_Id="0";
					
					$ionicLoading.show();

					SignupService.Signup($scope.Signform.email,$scope.Signform.password,$scope.Signform.gender,$scope.Signform.username,FB_Id).then(function (data) {
						$ionicLoading.hide();
						//alert(JSON.stringify(data));
						if(data.result){
							$scope.Signform={
								"username":"",
								"email":"",
								"password":"",
								"Cpassword":"",
								"gender":""
							};
							$scope.Signform.gender='male';
							$localstorage.set("UserId", data.data._id);
							$localstorage.set("FBId", data.data.fb_id);
							$localstorage.set("FullName", data.data.fullname);
							$localstorage.set("Email", data.data.email);
							$localstorage.set("avator", data.data.avator);
							
							//alert($localstorage.get("UserId")+" "+$localstorage.get("FBId")+" "+$localstorage.get("FullName")+" "+$localstorage.get("Email"));
							$state.go('nav.home');
							$ionicLoading.hide();
						}else{
							alert(data.msg);
						}
						$ionicLoading.hide();
					});	

				}else{

					alert("Password not match");
					return false;
				}
				
			};
			
			
			
			// Fb Login
			
			$scope.facebookLogin = function(){
				$ionicLoading.show();
				$cordovaFacebook.login(["public_profile", "email", "user_friends"])
					.then(function(success) {
						try{
							//$ionicLoading.hide();
							var access_token = success.authResponse.accessToken;
							var userID = success.authResponse.userID;
							
							var mainURL = "https://graph.facebook.com/"+userID+"?fields=id,name,email,gender&access_token="+access_token;

							$http({
								method: 'GET', 
								url: mainURL,
							}).success(function(data) {
								try{

									var name = data.name;
									var id = data.id;
									var email = data.email;
									var gender = data.gender;
									var password="";
									SignUpService.SignUp(email,password,gender,name,id).then(function (data) {
										$ionicLoading.hide();
										if(data.results=='OK'){
											$localstorage.set("UserId", data.data[0]._id);
											$localstorage.set("FBId", data.data[0].fb_id);
											$localstorage.set("FullName", data.data[0].fullname);
											$localstorage.set("Email", data.data[0].email);
											
											alert($localstorage.get("UserId")+" "+$localstorage.get("FBId")+" "+$localstorage.get("FullName")+" "+$localstorage.get("Email"));
											$state.go('nav.home');
											
											$ionicLoading.hide();
										}else{
											alert(data.msg);
										}
									});
								}
								catch(err){
									$ionicLoading.hide();
									console.log(JSON.stringify(err));
								}
							}).error(function(data, status, headers, config) {
								console.log(JSON.stringify(data));
								$ionicLoading.hide();
							});

						}catch(err){
							console.log(err.message)
						}
					}, function (error) {
						$ionicLoading.hide();
					});
			};
		}catch(err){
			console.log(err.message);
		}
	});
		   
});