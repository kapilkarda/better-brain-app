angular.module('login.controller', ['ionic'])
.controller('LoginPageController', function($scope, $state, $ionicPlatform, LoginService, SignupService, $localstorage, $ionicLoading, $ionicHistory, $cordovaFacebook, $http, $cordovaOauth) {
	$ionicPlatform.ready(function(){
		try{
			
			var userId = $localstorage.get("UserId");
			if(userId!=undefined && userId!=null && userId!="undefined" && userId!="null" && userId!="0" && userId!=""){
				$state.go('nav.home');
			}
			
			// Fb Login
			$scope.facebookLogin = function(){
				try{
					$ionicLoading.show();
					$cordovaOauth.facebook("146771425675194", ["email","public_profile", "user_friends"]).then(function(result) {
						$http.get("https://graph.facebook.com/me", { params: { access_token: result.access_token, fields: "id, name, email, gender, picture", format: "json" }}).then(function(result1) {
								var id = result1.data.id;
								var name = result1.data.name;
								var gender = result1.data.gender;
								var picture =result1.data.picture.data.url;
								var accessToken = result1.config.params.access_token;
								if(result1.data.email==undefined){
									var email = "";
									$ionicLoading.hide();
									alert("Email is not getting, please use classic login.");
								}else{
									var email = result1.data.email;
									var password="123NewFBLOGIN";
									SignupService.Signup(email,password,gender,name,id).then(function (data) {
										if(data.result){
											$ionicLoading.hide();
											$localstorage.set("UserId", data.data[0]._id);
											$localstorage.set("FBId", data.data[0].fb_id);
											$localstorage.set("FullName", data.data[0].fullname);
											$localstorage.set("Email", data.data[0].email);
											$localstorage.set("avator", picture);
											$state.go('nav.home');
										}else{
											alert(data.msg);
										}
									});
								}

						}, function(error) {
							$ionicLoading.hide();
							alert("There was a problem getting your profile. Check the logs for details.");
							console.log(error);
						});
					}, function(error) {
						$ionicLoading.hide();
						alert(JSON.stringify(error));
					});
				}catch(err){
					$ionicLoading.hide();
					alert(err.message);
				}
			};
			
			//Get Login
			$scope.loginform={
				"email":"",
				"password":""
			};
			$scope.DoLogin = function(){
					
				if($scope.loginform.email==""){
					alert("Email is required.");
					return false;
				}else{
					var email=$scope.loginform.email;
					var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if (!filter.test(email))
					{	
						alert("Please enter Valid email");
						return false;
					}
				}
				if($scope.loginform.password==""){
					alert("password is required.");
					return false;
				}

				$ionicLoading.show();
				LoginService.Login($scope.loginform.email,$scope.loginform.password).then(function (data) {
					if(data.result){
						$scope.loginform={
							"email":"",
							"password":""
						};
						$localstorage.set("UserId", data.data[0]._id);
						$localstorage.set("FBId", data.data[0].fb_id);
						$localstorage.set("FullName", data.data[0].fullname);
						$localstorage.set("Email", data.data[0].email);
						$localstorage.set("avator", data.data[0].avator);
						$state.go('nav.home');
						$ionicLoading.hide();
					}else{
						$ionicLoading.hide();
						alert("Invalid Candidate");
					}
				});
			};
				
		}catch(err){
			console.log(err.message);
		}
	});
		   
});