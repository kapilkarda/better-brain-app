angular.module('leaderboard.controllers', [])

.controller('LeaderboardCtrl', function($scope, $ionicPlatform, $ionicLoading, GetleaderboardService, $localstorage) {
	$ionicPlatform.ready(function(){
		try{
			/* $scope.onSlideMove = function(data){
				//alert("You have selected " + data.index + " tab");
			}; */
			var avator = $localstorage.get("avator");

			$scope.mainUserId = $localstorage.get("UserId");

			$scope.leaderboards=[];
			$scope.Leaderboard=function(){
				$ionicLoading.show();
				GetleaderboardService.Getleaderscore().then(function (data) {
					if(data.data!=''){
						console.log(data);
						$scope.leaderboards=data.data;
						$ionicLoading.hide();
					}else{
						$ionicLoading.hide();
						alert("Please Try Later...");
					}
				});	
			};
			$scope.Leaderboard();

			/* Tabs js start*/
				
			/* Tabs js close*/


		}catch(err){
			alert(err.message);
		}
	});
});
