angular.module('BetterBrain')
.service('QuestionService', function ($q,$http,API) {

    function getQuestions(topicId){
		  
      return $http.get(API.getQuestions(topicId)).then(function(response){
        return response.data
      });
    }

    return {
      getQuestions:  getQuestions
    }

  });