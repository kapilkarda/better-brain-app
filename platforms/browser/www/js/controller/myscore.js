angular.module('myscore.controllers', [])

.controller('MyscoreCtrl', function($scope, $ionicPlatform, $localstorage, $ionicLoading, GetScoreService) {
	$ionicPlatform.ready(function(){
		try{
			$scope.userId = $localstorage.get("UserId");
			$scope.myScore=0;
			$scope.MyScores=function(){
				$ionicLoading.show();
				GetScoreService.GetScore($scope.userId).then(function (data) {
					if(data.data!=''){
						for(var i=0;i<data.data.length;i++){
							$scope.myScore=Number($scope.myScore)+Number(data.data[i].point);
						}
						//alert(JSON.stringify($scope.myScores));
						$ionicLoading.hide();
					}else{
						$ionicLoading.hide();
						alert("Please Try Letter...");
					}
				});	
			};
			$scope.MyScores();
		}catch(err){
			alert(err.message);
		}
	});
});
