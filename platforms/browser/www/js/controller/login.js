angular.module('login.controller', ['ionic'])
.controller('LoginPageController', function($scope, $state, $ionicPlatform, LoginService, SignupService, $localstorage, $ionicLoading, $ionicHistory, $cordovaFacebook, $http) {
	$ionicPlatform.ready(function(){
		try{
			
			var userId = $localstorage.get("UserId");
			if(userId!=undefined && userId!=null && userId!="undefined" && userId!="null" && userId!="0" && userId!=""){
				$state.go('nav.home');
			}
			
			// Fb Login
			$scope.facebookLogin = function(){
				try{
					$ionicLoading.show();
					$cordovaFacebook.login(["public_profile", "email", "user_friends"])
						.then(function(success) {
							try{
								var access_token = success.authResponse.accessToken;
								var userID = success.authResponse.userID;
								var mainURL = "https://graph.facebook.com/"+userID+"?fields=id,name,email,gender&access_token="+access_token;

								$http({
									method: 'GET', 
									url: mainURL,
								}).success(function(data) {
									try{

										var name = data.name;
										var id = data.id;
										var email = data.email;
										var gender = data.gender;
										var password="123NewFBLOGIN";

										SignupService.Signup(email,password,gender,name,id).then(function (data) {
											$ionicLoading.hide();
											if(data.result){
												$localstorage.set("UserId", data.data[0]._id);
												$localstorage.set("FBId", data.data[0].fb_id);
												$localstorage.set("FullName", data.data[0].fullname);
												$localstorage.set("Email", data.data[0].email);
												$state.go('nav.home');
											}else{
												alert(data.msg);
											}
											
										});

									}
									catch(err){
										$ionicLoading.hide();
										console.log(err.message);
									}
								}).error(function(data, status, headers, config) {
									console.log(JSON.stringify(data));
									$ionicLoading.hide();
								});

							}catch(err){
								console.log(err.message)
							}
						}, function (error) {
							$ionicLoading.hide();
						});
				}catch(err){
					$ionicLoading.hide();
					console.log(err.message);
				}
			};
			
			//Get Login
			$scope.loginform={
				"email":"",
				"password":""
			};
			$scope.DoLogin = function(){
					
				if($scope.loginform.email==""){
					alert("Email is required.");
					return false;
				}else{
					var email=$scope.loginform.email;
					var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if (!filter.test(email))
					{	
						alert("Please enter Valid email");
						return false;
					}
				}
				if($scope.loginform.password==""){
					alert("password is required.");
					return false;
				}

				$ionicLoading.show();
				LoginService.Login($scope.loginform.email,$scope.loginform.password).then(function (data) {
					if(data.result){
						$scope.loginform={
							"email":"",
							"password":""
						};
						$localstorage.set("UserId", data.data[0]._id);
						$localstorage.set("FBId", data.data[0].fb_id);
						$localstorage.set("FullName", data.data[0].fullname);
						$localstorage.set("Email", data.data[0].email);
						$state.go('nav.home');
						$ionicLoading.hide();
					}else{
						$ionicLoading.hide();
						alert("Invalid Candidate");
					}
				});
				//$state.go('nav.home');
			};
				
		}catch(err){
			console.log(err.message);
		}
	});
		   
});