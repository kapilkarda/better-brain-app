angular.module('leaderboard.controllers', [])

.controller('LeaderboardCtrl', function($scope, $ionicPlatform, $ionicLoading, GetleaderboardService) {
	$ionicPlatform.ready(function(){
		try{
			/* $scope.onSlideMove = function(data){
				//alert("You have selected " + data.index + " tab");
			}; */
			$scope.leaderboards=[];
			$scope.Leaderboard=function(){
				$ionicLoading.show();
				GetleaderboardService.Getleaderscore().then(function (data) {
					if(data.data!=''){
						$scope.leaderboards=data.data;
						$ionicLoading.hide();
					}else{
						$ionicLoading.hide();
						alert("Please Try Letter...");
					}
				});	
			};
			$scope.Leaderboard();
		}catch(err){
			alert(err.message);
		}
	});
});
